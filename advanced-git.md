## Команди по роботі з гілками:
```
git branch

git branch new-branch
git checkout new-branch

git checkout -b new-branch
```

## Завдання
1. Створити нову гілку
2. Внести правки по коду
3. Запушити зміни
4. Створити **Merge Request** в `master`
5. Замерджити зміни в `master`
6. Переглянути зміни на **gitlab** та локально
