# План
1. [Налаштування та знайомство з git та gitlab](git.md)
   1. [Клонувати існуючий репозиторій](task-1.md)
   2. [Створити власний репозиторій для класних робіт](task-2.md)
   3. [Створити власний репозиторій для домашніх робіт](task-3.md)
2. Робота з git за допомогою інтерфейсу VS Code / WebStorm / IntelliJ IDEA
3. [Налаштування плагінів для VS Code](vscode-plugins.md)
4. Знайомство з Figma
5. [Просунуті практики з git](advanced-git.md)

# References
[Сброс сохраненного пароля в Windows](https://dan-it.gitlab.io/fe-book/git/git_reset_password.html)