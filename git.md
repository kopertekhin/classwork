### Перевірка наявності git
```
git --version
```

### Довідка
```
git --help
git add --help
```

### Глобальні налаштування 
#### Задати налаштування
```
git config --global user.name "Rostislav Svidelskyi"
git config --global user.email sv.rostislav@gmail.com
```
#### Перевірити налаштування 
```
git config --global user.name
git config --global user.email
```

### Ініціалізація репозиторію в існуючій директорії
```
git init
```

### Перевірка стану змін
```
git status
```

### Переміщення файлів в статус **staged**
```
git add .
```

### Додавання файлів до історії
```
git commit -m 'initial commit'
```

### Перегляд історії комітів
```
git log 
```

### Пуш змін до віддаленого репозиторію
```
git push
```
